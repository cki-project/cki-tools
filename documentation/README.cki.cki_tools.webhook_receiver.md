---
title: cki.cki_tools.webhook_receiver
linkTitle: webhook_receiver
description: Webhook receiver for GitLab, Sentry and Jira
---

The webhook receiver can post events received from GitLab and Sentry.io to an
AMQP message bus.

| Environment variable                       | Description                                         |
|--------------------------------------------|-----------------------------------------------------|
| `RABBITMQ_HOST`                            | AMQP host                                           |
| `RABBITMQ_PORT`                            | AMQP port, TLS is used for port 443                 |
| `RABBITMQ_USER`                            | AMQP user                                           |
| `RABBITMQ_PASSWORD`                        | AMQP password                                       |
| `RABBITMQ_EXCHANGE`                        | AMQP exchange for the posted messages               |
| `RABBITMQ_CAFILE`                          | AMQP CA file path                                   |
| `RABBITMQ_CERTFILE`                        | AMQP certificate + private key file path            |
| `RABBITMQ_KEEPALIVE_S`                     | Time to keep AMQP connection alive between messages |
| `WEBHOOK_RECEIVER_WEBSECRET`               | GitLab webhook secret                               |
| `WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET` | Sentry.io webhook client secret                     |

## GitLab integration

Go to <https://gitlab.com/your-project/hooks>, and add a new webhook for
<https://webhook-receiver-host/>, the secret from the `WEBHOOK_RECEIVER_WEBSECRET`
environment variable and all required trigger types.

Messages posted to the exchange will use a routing key of
`hostname.project.event`, e.g.
`gitlab.com.cki-project.kernel-ark.merge_request`.

## Sentry.io integration

Go to Settings -> Organization -> Developer Settings, create a new internal
integration with a webhook URL of <https://webhook-receiver-host/sentry>, and
enable `Alert Rule Action` there. Save the client secret in the
`WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET` environment variable.

For each project where events should be forwarded to the message bus, go to the
Alerts page and Create an alert rule with `Issue Alert`, when `an event is
seen` and `send a notification via an integration` via the integration defined
above.

Messages posted to the exchange will use a routing key of
`sentry.io.project.resource.action`, e.g.
`sentry.io.webhook-receiver.event_alert.triggered`.
