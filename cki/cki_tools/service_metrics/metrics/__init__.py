"""Actual metrics."""

from .aws import AwsMetricsDaily
from .aws import AwsMetricsMinutely
from .beaker import BeakerMetrics
from .beakerdb import BeakerDBMetrics
from .gitlab import GitLabMetricsHourly
from .gitlab import GitLabMetricsMinutely
from .kubernetes import KubernetesMetrics
from .s3buckets import S3BucketMetrics
from .sentry import SentryMetrics
from .volume import VolumeMetrics

ALL_METRICS = [
    AwsMetricsDaily,
    AwsMetricsMinutely,
    BeakerDBMetrics,
    BeakerMetrics,
    GitLabMetricsHourly,
    GitLabMetricsMinutely,
    KubernetesMetrics,
    S3BucketMetrics,
    SentryMetrics,
    VolumeMetrics,
]
