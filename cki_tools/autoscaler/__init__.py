"""Monitor queues and scale up/down services."""

import argparse
import dataclasses
from importlib import resources
import itertools
import os
import pathlib
import time
import typing

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib import yaml
from cki_lib.logger import get_logger
from kubernetes import client
from kubernetes import config as k8s_config
from kubernetes import dynamic

LOGGER = get_logger('cki_tools.autoscaler')


@dataclasses.dataclass
class ServiceData:  # pylint: disable=too-many-instance-attributes
    """Service to monitor."""

    name: str
    message_baseline: int
    messages_per_replica: int
    max_scale_down_step: int
    max_scale_up_step: int
    queue_prefix: str
    replicas_max: int
    replicas_min: int

    def new_replicas(self, messages: int, replicas: int) -> int:
        """Calculate the new number of replicas."""
        result = (
            min(self.replicas_max, (
                max(self.replicas_min, (
                    (
                        max(0, messages - self.message_baseline)
                        +
                        self.messages_per_replica - 1
                    )
                    //
                    self.messages_per_replica
                ))
            ))
        )

        if result > replicas:
            LOGGER.debug('%s: should scale up', self.name)
            result = min(result, replicas + self.max_scale_up_step)
        elif result < replicas:
            LOGGER.debug('%s: should scale down', self.name)
            result = max(result, replicas - self.max_scale_down_step)

        return result


class Autoscaler:  # pylint: disable=too-few-public-methods
    """Check whether deployments need scaling."""

    def __init__(
        self,
        namespaces: list[str],
        queue: messagequeue.MessageQueue,
        config: dict[str, typing.Any],
    ) -> None:
        """Initialize service."""
        self.namespaces = namespaces
        self.queue = queue
        deploy_suffix = '' if misc.is_production() else '-staging'
        self.services = {n + deploy_suffix: ServiceData(name=n, **c) for n, c in config.items()}

        k8s_config.load_incluster_config()
        self.client = dynamic.client.DynamicClient(client.ApiClient()).resources.get(
            api_version='apps/v1', kind='Deployment')

    def run(self, refresh_period: int) -> None:
        """Run the checks and scale up/down if necessary."""
        while True:
            for deploy in itertools.chain.from_iterable(
                    self.client.get(namespace=n)['items'] for n in self.namespaces):
                if not (service := self.services.get(deploy.metadata.name)):
                    continue
                queue_name = service.queue_prefix + service.name
                with self.queue.connect() as channel:
                    messages = channel.queue_declare(queue_name, passive=True).method.message_count
                replicas = deploy.spec.replicas
                LOGGER.debug('%s: %d messages, %d replicas', service.name, messages, replicas)

                if (new_replicas := service.new_replicas(messages, replicas)) == replicas:
                    LOGGER.debug('%s: replica count not changed', service.name)
                    continue

                LOGGER.info('scaling %s application=%s messages=%d replicas=%d->%d',
                            'up' if new_replicas > replicas else 'down',
                            service.name, messages, replicas, new_replicas)
                if misc.is_production_or_staging():
                    self.client.server_side_apply(
                        namespace=deploy.metadata.namespace, name=f'{deploy.metadata.name}/scale',
                        field_manager='autoscaler', force_conflicts=True,
                        body={
                            'apiVersion':  'autoscaling/v1', 'kind': 'Scale',
                            'spec': {'replicas': new_replicas},
                        },
                    )

            time.sleep(refresh_period)


def main(args: list[str] | None = None) -> None:
    """CLI Interface."""
    parser = argparse.ArgumentParser()
    parser.parse_args(args)

    metrics.prometheus_init()

    namespaces = (os.environ.get('AUTOSCALER_NAMESPACES') or pathlib.Path(
        '/var/run/secrets/kubernetes.io/serviceaccount/namespace'
    ).read_text(encoding='utf8')).split(',')

    refresh_period = misc.get_env_int('AUTOSCALER_REFRESH_PERIOD', 30)
    queue = messagequeue.MessageQueue(keepalive_s=2 * refresh_period)

    schema_path = resources.files(__package__) / 'schema.yml'
    config = yaml.load(
        schema_path=schema_path,
        contents=os.environ.get('AUTOSCALER_CONFIG'),
        file_path=os.environ.get('AUTOSCALER_CONFIG_PATH'),
    )

    Autoscaler(namespaces, queue, config).run(refresh_period)
