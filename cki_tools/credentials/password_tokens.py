"""Generic password management."""

import subprocess
import typing

from . import token


@token.register_token('password')
class Password(token.Token):
    """Password."""

    clean_active_versions = 1

    def _create_token(self, token_version: str, meta: dict[str, typing.Any]) -> str:
        """Create a password."""
        return subprocess.run([
            'diceware', '--wordlist=en_eff', '--delimiter=', '--num=6', '--caps',
        ], encoding='utf8', check=True, stdout=subprocess.PIPE).stdout.strip()

    def _destroy_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Destroy a token version."""
        # nothing to do to destroy a password
