"""Import all available tokens."""

from . import aws_tokens  # noqa: F401, pylint: disable=unused-import
from . import dogtag_tokens  # noqa: F401, pylint: disable=unused-import
from . import gitlab_tokens  # noqa: F401, pylint: disable=unused-import
from . import hv_tokens  # noqa: F401, pylint: disable=unused-import
from . import ldap_tokens  # noqa: F401, pylint: disable=unused-import
from . import password_tokens  # noqa: F401, pylint: disable=unused-import
from . import splunk_tokens  # noqa: F401, pylint: disable=unused-import
from . import ssh_tokens  # noqa: F401, pylint: disable=unused-import
