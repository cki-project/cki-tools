"""Tests for s3-proxy image."""

from collections import abc
import contextlib
import os

import boto3
import botocore
from cki_lib import misc
from cki_lib import session
from cki_lib.inttests import cluster
from cki_lib.inttests.minio import MinioServer


@cluster.skip_without_requirements()
class TestS3Proxy(MinioServer):
    """Tests for s3-proxy image."""

    def setUp(self) -> None:
        """Set up the service per test."""
        super().setUp()
        self.enterContext(self.k8s_namespace('image-under-test'))
        self.enterContext(self._service())

    def test_smoke(self) -> None:
        """Basic smoke tests."""
        bucket = 'integration-test'
        key0 = 'index.html'
        key1 = 'path/to/file with spaces.txt'
        key2 = 'some-path/file.txt'
        key3 = 'another path/to/file with spaces.txt'
        key4 = 'some-path/index.html'
        key5 = 'ab>-base64-with-plus'  # base64 encoding contains plus: YWI+LWJhc2U2NC13aXRoLXBsdXM=
        body = b'Hello World'

        minio_client = boto3.session.Session().client(
            's3', endpoint_url=os.environ['AWS_ENDPOINT_URL'])
        minio_client.create_bucket(Bucket=bucket)
        minio_client.put_object(Bucket=bucket, Key=key0, Body=body)
        minio_client.put_object(Bucket=bucket, Key=key1, Body=body)
        minio_client.put_object(Bucket=bucket, Key=key2, Body=body)
        minio_client.put_object(Bucket=bucket, Key=key3, Body=body)
        minio_client.put_object(Bucket=bucket, Key=key4, Body=body)
        minio_client.put_object(Bucket=bucket, Key=key5, Body=body)

        proxy_client = boto3.session.Session().client(
            's3', endpoint_url=f'http://{self.hostname}:8000',
            config=botocore.client.Config(signature_version=botocore.UNSIGNED))
        proxy_buckets = {b['Name'] for b in proxy_client.list_buckets()['Buckets']}
        proxy_all_objects = {o['Key'] for o in proxy_client.list_objects(
            Bucket=bucket)['Contents']}
        proxy_prefixed_objects = {o['Key'] for o in proxy_client.list_objects(
            Bucket=bucket, Prefix='path/')['Contents']}
        proxy_prefixes = {p['Prefix'] for p in proxy_client.list_objects(
            Bucket=bucket, Delimiter='/')['CommonPrefixes']}

        with self.subTest('buckets'):
            self.assertEqual(proxy_buckets, {bucket})
        with self.subTest('list objects'):
            self.assertEqual(proxy_all_objects, {key0, key1, key2, key3, key4, key5})
        with self.subTest('list prefixed objects'):
            self.assertEqual(proxy_prefixed_objects, {key1})
        with self.subTest('list prefixes'):
            self.assertEqual(proxy_prefixes, {'path/', 'some-path/', 'another path/'})

        requests_session = session.get_session(__name__, raise_for_status=True)
        for key in (key0, key4):
            with self.subTest('get', key=key):
                response = requests_session.get(f'http://{self.hostname}:8000/{bucket}/{key}')
                self.assertFalse(response.url.startswith(os.environ['AWS_ENDPOINT_URL']))
                self.assertEqual(response.content, body)
        for key in (key1, key2, key3):
            with self.subTest('get', key=key):
                response = requests_session.get(f'http://{self.hostname}:8000/{bucket}/{key}')
                self.assertTrue(response.url.startswith(os.environ['AWS_ENDPOINT_URL']))
                self.assertEqual(response.content, body)
        with self.subTest('list-no-slash'):
            response = requests_session.get(f'http://{self.hostname}:8000/{bucket}')
            self.assertFalse(response.url.startswith(os.environ['AWS_ENDPOINT_URL']))
        with self.subTest('list-with-slash'):
            response = requests_session.get(f'http://{self.hostname}:8000/{bucket}/')
            self.assertFalse(response.url.startswith(os.environ['AWS_ENDPOINT_URL']))

        # Continuation tokens can have '+' in them. If they are improperly encoded, they will be
        # interpreted as spaces. Minio uses the file name as the first part of the continuation
        # token, so we can use this to test that the continuation token is properly encoded.
        # The any_with_plus check is to ensure that the test actually tests the right thing.
        with self.subTest('continuation tokens'):
            list_args = {'Bucket': bucket, 'MaxKeys': 1}
            objects = set()
            any_with_plus = False
            while True:
                response = proxy_client.list_objects_v2(**list_args)
                objects |= {c['Key'] for c in response.get('Contents', [])}
                if not response.get('IsTruncated'):
                    break
                list_args['ContinuationToken'] = response['NextContinuationToken']
                if '+' in response['NextContinuationToken']:
                    any_with_plus = True
            self.assertTrue(any_with_plus)
            self.assertEqual(objects, {key0, key1, key2, key3, key4, key5})

    @contextlib.contextmanager
    def _service(self) -> abc.Iterator[None]:
        service_id = 'image-under-test'
        now = misc.now_tz_utc()
        self.k8s_deployment(namespace=service_id, name=service_id, setup_at=now, container={
            'name': 'default',
            'image': 'quay.io/cki/s3-proxy',
            'startupProbe': self.k8s_startup_probe(8000),
            'env': [
                {'name': 'AWS_ENDPOINT_HOST', 'value': 'minio.minio.svc.cluster.local:9000'},
                {'name': 'AWS_ENDPOINT_HOST_EXTERNAL', 'value': f'{self.hostname}:9000'},
                {'name': 'AWS_ENDPOINT_SCHEME', 'value': 'http'},
                {'name': 'AWS_ACCESS_KEY_ID', 'value': os.environ['AWS_ACCESS_KEY_ID']},
                {'name': 'AWS_SECRET_ACCESS_KEY', 'value': os.environ['AWS_SECRET_ACCESS_KEY']},
                {'name': 'AWS_DEFAULT_REGION', 'value': os.environ['AWS_DEFAULT_REGION']},
            ],
        }, image_under_test=True)
        self.k8s_service(namespace=service_id, name=service_id)
        if not self.k8s_wait(namespace=service_id, name=service_id, setup_at=now):
            raise Exception(f'{service_id} did not start up')

        yield
