"""Webhook interaction tests."""
import unittest
from unittest import mock

from cki_tools.slack_bot import webhook


class TestWebhook(unittest.TestCase):
    """ Test webhooks."""

    def setUp(self):
        """SetUp class."""
        self.client = webhook.app.test_client()

    def test_index(self):
        """Check the index page."""
        response = self.client.get('/')
        self.assertEqual(response.status, '200 OK')

    @mock.patch('cki_tools.slack_bot.slack.send_message')
    def test_message(self, send_message):
        """Check routing of a message."""
        response = self.client.post(
            '/message', json={'message': 'content'})
        self.assertEqual(response.status, '200 OK')
        send_message.assert_called_with('content')

    @mock.patch('cki_tools.slack_bot.slack.send_message')
    def test_alertmanager(self, send_message):
        """Check routing of a Grafana alert."""
        response = self.client.post(
            '/alertmanager', json={
                "externalURL": "https://externalurl",
                "alerts": [{
                    "status": "firing",
                    "labels": {"alertname": "name1"},
                    "annotations": {"summary": "summary1"},
                }, {
                    "status": "resolved",
                    "labels": {"alertname": "name2"},
                    "annotations": {"summary": "summary2",
                                    "dashboard": "https://dashboard2"},
                }]})
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(len(send_message.mock_calls), 2)

        self.assertIn('RESOLVED', send_message.mock_calls[0].args[0])
        self.assertIn('name2', send_message.mock_calls[0].args[0])
        self.assertIn('summary2', send_message.mock_calls[0].args[0])
        self.assertIn('dashboard2', send_message.mock_calls[0].args[0])

        self.assertIn('externalurl', send_message.mock_calls[1].args[0])
        self.assertIn('FIRING', send_message.mock_calls[1].args[0])
        self.assertIn('name1', send_message.mock_calls[1].args[0])
        self.assertIn('summary1', send_message.mock_calls[1].args[0])
