"""Tests for gitlab codeowner config."""

import contextlib
import io
import json
import pathlib
import tempfile
import typing
import unittest
from unittest import mock

import responses

from cki_tools.gitlab_codeowners_config import join_lines
from cki_tools.gitlab_codeowners_config import main


@mock.patch.dict('os.environ', {'FOOTER_CONFIG': 'name: foo'})
class TestCodeOwnerConfig(unittest.TestCase):
    """Tests for gitlab codeowner config."""

    @staticmethod
    def _main(
        config: typing.List[str],
        mode: str,
        /,
        tests: typing.Any = None,
        args: typing.Optional[typing.List[str]] = None,
    ) -> typing.Tuple[int, str]:
        """Run the main program."""
        args = args or []
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'conf').write_text(join_lines(config), encoding='utf8')
            if tests:
                pathlib.Path(directory, 'tests.json').write_text(json.dumps(tests), encoding='utf8')
                args += ['--test-path', f'{directory}/tests.json']
            args += ['--config-path', f'{directory}/conf', mode]
            if mode in {'generate', 'diff'}:
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    result = main(args)
                return result, stdout.getvalue().replace(directory, 'DIRECTORY')
            if mode == 'update':
                result = main(args)
                output = pathlib.Path(directory, 'conf').read_text(encoding='utf8')
                return result, output.replace(directory, 'DIRECTORY')
            if mode == 'needs-update':
                return main(args), ''
            raise Exception(f'unknown mode {mode}')

    @responses.activate
    def test_generate(self) -> None:
        """Test generating a simple codeowners file."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30},
                            {'username': 'user2', 'access_level': 20}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '#     - user3',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'generate'), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     paths:\n'
            '#     - path\n'
            '#     users:\n'
            '#     - user1\n'
            '#     - user2\n'
            '#     - user3\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# 4be0b4e8cb900b0de20a3233833bef63ce365695c5d4abe2f364b5f334240d57\n'
            '\n'
            '[section]\n'
            'path @user1\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_group(self) -> None:
        """Test a group reference."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/groups/group1/members',
                      json=[{'username': 'user1'}, {'username': 'user2'}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - group1',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'generate'), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     paths:\n'
            '#     - path\n'
            '#     users:\n'
            '#     - group1\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# e738d28c3b4d366eb70fc6bc3727235297f3f422020df9a4f118312cf0d9a452\n'
            '\n'
            '[section]\n'
            'path @user1\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_kpet_maintainers(self) -> None:
        """Test kpet maintainers."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30},
                            {'username': 'user2', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/groups/group1/members',
                      json=[{'username': 'user1'}, {'username': 'user2'}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     kpet: maintainers',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'generate', tests=[
            {'origin': 'kernel_public_tests', 'location': 'path',
                "maintainers": [{"gitlab": "user1"}, {}]},
            {'origin': 'kernel_public_tests', 'location': 'path',
                "maintainers": [{"gitlab": "group1"}]},
            {'origin': 'foo', 'location': 'path2', "maintainers": [{"gitlab": "user1"}]},
        ]), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     kpet: maintainers\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# 21342a689bde337fd2e598871608d307ec00aaa5c8214e13a5b92b0455b930d0\n'
            '\n'
            '[section]\n'
            '/path/ @user1 @user2\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_kpet_maintainers_count(self) -> None:
        """Test kpet maintainers with a minimum count."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30},
                            {'username': 'user2', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/groups/group1/members',
                      json=[{'username': 'user1'}, {'username': 'user2'}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     minimum_count: 2',
            '#     kpet: maintainers',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'generate', tests=[
            {'origin': 'kernel_public_tests', 'location': 'path1',
                "maintainers": [{"gitlab": "user1"}]},
            {'origin': 'kernel_public_tests', 'location': 'path2',
                "maintainers": [{"gitlab": "group1"}]},
        ]), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     minimum_count: 2\n'
            '#     kpet: maintainers\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# 0f93ec5262d0761607351a75f6c4a3029d4f6f3bb5fa5ccd767d8b5a7d80ac33\n'
            '\n'
            '[section]\n'
            '/path2/ @user1 @user2\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_kpet_maintainers_additional(self) -> None:
        """Test kpet maintainers with additional owners."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30},
                            {'username': 'user2', 'access_level': 30}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     users:',
            '#     - user2',
            '#     kpet: maintainers',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'generate', tests=[
            {'origin': 'kernel_public_tests', 'location': 'path',
                "maintainers": [{"gitlab": "user1"}]},
        ]), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     users:\n'
            '#     - user2\n'
            '#     kpet: maintainers\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# 40084b1a803db0db891da6ead20b58ada4c93c0b49603aa2d24bcc4ab0797a64\n'
            '\n'
            '[section]\n'
            '/path/ @user1 @user2\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_kpet_locations(self) -> None:
        """Test kpet locations."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     users:',
            '#     - user1',
            '#     kpet: locations',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'generate', tests=[
            {'origin': 'kernel_public_tests', 'location': 'path'},
            {'origin': 'foo', 'location': 'path2'}
        ]), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     users:\n'
            '#     - user1\n'
            '#     kpet: locations\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# b5ee51aaa07765dc869fd6a4bc5805431767496e6c78015d980b3cf633a66f87\n'
            '\n'
            '[section]\n'
            '/path/ @user1\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_update(self) -> None:
        """Test updating a simple codeowners file."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user2',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'update'), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     paths:\n'
            '#     - path\n'
            '#     users:\n'
            '#     - user1\n'
            '#     - user2\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01\n'
            '\n'
            '[section]\n'
            'path @user1\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_update_new(self) -> None:
        """Test updating a simple codeowners file that has no generated content before."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'update'), (
            0,
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     paths:\n'
            '#     - path\n'
            '#     users:\n'
            '#     - user1\n'
            '#     - user2\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01\n'
            '\n'
            '[section]\n'
            'path @user1\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_update_manual(self) -> None:
        """Test updating a simple codeowners file that has manual content before."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])

        self.assertEqual(self._main([
            '[manual-section]',
            'manual-path @manual-user',
            '',
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
        ], 'update'), (
            0,
            '[manual-section]\n'
            'manual-path @manual-user\n'
            '\n'
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN\n'
            '# general:\n'
            '#   access_level: 30\n'
            '#   project_url: https://url/project\n'
            '# sections:\n'
            '#   section:\n'
            '#     paths:\n'
            '#     - path\n'
            '#     users:\n'
            '#     - user1\n'
            '#     - user2\n'
            '# GITLAB-CODEOWNERS-CONFIG-END\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN\n'
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01\n'
            '\n'
            '[section]\n'
            'path @user1\n'
            '\n'
            '# GITLAB-CODEOWNERS-GENERATED-END\n'))

    @responses.activate
    def test_diff(self) -> None:
        """Test diffing a simple codeowners file."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])

        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[])
        responses.add(responses.POST, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json={})

        result = (
            '@@ -15,6 +15,6 @@\n'
            ' # e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01\n'
            ' \n'
            ' [section]\n'
            '-path @user2\n'
            '+path @user1\n'
            ' \n'
            ' # GITLAB-CODEOWNERS-GENERATED-END\n'
        )

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user2',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'diff', args=['--comment-mr-iid', '1']), (
            1,
            '--- DIRECTORY/conf\n'
            '+++ DIRECTORY/conf\n'
            + result
        ))
        self.assertIn(result, json.loads(responses.calls[-1].request.body)['body'])

    @responses.activate
    def test_diff_note(self) -> None:
        """Test updating a note."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])

        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[
                          {'id': 1, 'system': True,  'author': {'username': 'bot'}},
                          {'id': 2, 'system': False, 'author': {'username': 'human'}},
                          {'id': 3, 'system': False, 'author': {'username': 'bot'}},
                      ])
        responses.add(responses.DELETE, 'https://url/api/v4/projects/1/merge_requests/1/notes/3',
                      json={})
        responses.add(responses.POST, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json={})

        result = (
            '@@ -15,6 +15,6 @@\n'
            ' # e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01\n'
            ' \n'
            ' [section]\n'
            '-path @user2\n'
            '+path @user1\n'
            ' \n'
            ' # GITLAB-CODEOWNERS-GENERATED-END\n'
        )

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user2',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'diff', args=['--comment-mr-iid', '1']), (
            1,
            '--- DIRECTORY/conf\n'
            '+++ DIRECTORY/conf\n'
            + result
        ))
        self.assertEqual(responses.calls[-2].request.method, responses.DELETE)
        self.assertIn(result, json.loads(responses.calls[-1].request.body)['body'])

    @responses.activate
    def test_diff_no_difference(self) -> None:
        """Test diffing a simple codeowners file with the same result."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user1',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'diff',  args=['--comment-mr-iid', '1']), (0, ''))

    @responses.activate
    def test_diff_no_difference_note(self) -> None:
        """Test diffing a simple codeowners file with the same result."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[
                          {'id': 1, 'system': True,  'author': {'username': 'bot'}},
                          {'id': 2, 'system': False, 'author': {'username': 'human'}},
                          {'id': 3, 'system': False, 'author': {'username': 'bot'}},
                      ])
        responses.add(responses.DELETE, 'https://url/api/v4/projects/1/merge_requests/1/notes/3',
                      json={})

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user1',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'diff',  args=['--comment-mr-iid', '1']), (0, ''))
        self.assertEqual(responses.calls[-1].request.method, responses.DELETE)

    @responses.activate
    def test_needs_update_matches(self) -> None:
        """Test a matching checksum."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user2',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'needs-update',  args=['--comment-mr-iid', '1']), (0, ''))

    @responses.activate
    def test_needs_update_matches_note(self) -> None:
        """Test note removal on matching checksum."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[
                          {'id': 1, 'system': True,  'author': {'username': 'bot'}},
                          {'id': 2, 'system': False, 'author': {'username': 'human'}},
                          {'id': 3, 'system': False, 'author': {'username': 'bot'}},
                      ])
        responses.add(responses.DELETE, 'https://url/api/v4/projects/1/merge_requests/1/notes/3',
                      json={})

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# e4c3104058b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user2',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'needs-update', args=['--comment-mr-iid', '1']), (0, ''))
        self.assertEqual(responses.calls[-1].request.method, responses.DELETE)

    @responses.activate
    def test_needs_update_differs(self) -> None:
        """Test a wrong checksum."""
        responses.add(responses.GET, 'https://url/api/v4/projects/project',
                      json={'id': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/members/all?per_page=100',
                      json=[{'username': 'user1', 'access_level': 30}])
        responses.add(responses.GET, 'https://url/api/v4/user',
                      json={'username': 'bot', 'web_url': 'https://url/bot'})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1',
                      json={'iid': 1})
        responses.add(responses.GET, 'https://url/api/v4/projects/1/merge_requests/1/notes',
                      json=[])

        self.assertEqual(self._main([
            '# GITLAB-CODEOWNERS-CONFIG-BEGIN',
            '# general:',
            '#   access_level: 30',
            '#   project_url: https://url/project',
            '# sections:',
            '#   section:',
            '#     paths:',
            '#     - path',
            '#     users:',
            '#     - user1',
            '#     - user2',
            '# GITLAB-CODEOWNERS-CONFIG-END',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-BEGIN',
            '# deadbeef58b4f203e34fc4336b6fde234386a00f8924c08226842e25068aed01',
            '',
            '[section]',
            'path @user2',
            '',
            '# GITLAB-CODEOWNERS-GENERATED-END',
        ], 'needs-update',  args=['--comment-mr-iid', '1']), (1, ''))
