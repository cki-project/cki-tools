"""Test cki_tools.render module."""

import contextlib
import io
import itertools
import os
import pathlib
import tempfile
import typing
import unittest
from unittest import mock

from cki_lib import yaml
import responses

from cki_tools import render
from cki_tools.credentials import secrets


class TestJinja2(unittest.TestCase):
    """Test jinja2 rendering."""

    def _test(
        self,
        template,
        output,
        *,
        cli_args=None,
        arg=None,
        data_files=None,
        data=None,
        raw_data=None,
        include_paths=None,
        stdin=False,
    ):
        # pylint: disable=too-many-arguments
        with tempfile.TemporaryDirectory() as temp_directory:
            temp_dir = pathlib.Path(temp_directory)
            render_args = []

            if stdin:
                stdin_mock = mock.patch('sys.stdin', io.StringIO(template))
            else:
                template_file = temp_dir / 'template.txt'
                template_file.write_text(template)
                render_args += [template_file.as_posix()]
                stdin_mock = contextlib.nullcontext()

            exception_cm = self.assertRaises(Exception) if not output else contextlib.nullcontext()

            output_file = temp_dir / 'output.txt'

            for name, contents in (data_files or {}).items():
                data_file: pathlib.Path = temp_dir / name
                data_file.parent.mkdir(parents=True, exist_ok=True)
                data_file.write_text(contents)

            render_args += list(itertools.chain(
                cli_args or ['--no-autoescape'],
                ['--output', output_file.as_posix()],
                *(['--arg', f'{k}={v}'] for k, v in (arg or {}).items()),
                *(['--data', f'{k}={temp_dir / v}'] for k, v in (data or {}).items()),
                *(['--raw-data', f'{k}={temp_dir / v}'] for k, v in (raw_data or {}).items()),
                *(['--include-path', f'{temp_dir / p}'] for p in (include_paths or []))))

            with stdin_mock, exception_cm:
                render.main(render_args)
            if output:
                self.assertEqual(output_file.read_text(), output)

    @mock.patch.dict('cki_tools.render.os.environ', {'FOO': 'BAR'})
    def test_filters(self) -> None:
        """Test filters."""
        cases = [
            ('"$FOO" | env', 'BAR'),
            ('"${FOO}" | env', 'BAR'),
            ('"${FOO" | env', '${FOO'),
            ('"$FOO}" | env', 'BAR}'),
            ('"{FOO}" | env', '{FOO}'),
            ('"#FOO" | env', '#FOO'),
            ('"$NOT_DEFINED_VAR" | env', None),
            ('"true" | is_true', 'True'),
            ('"false" | is_true', 'False'),
            ('"pooh: bear" | from_yaml | tojson', '{"pooh": "bear"}'),
            ('"PoohBear" | to_kebab_case', 'pooh-bear'),
            ('"pooh-bear" | to_pascal_case', 'PoohBear'),
            ('"PoohBear" | to_snake_case', 'pooh_bear'),
            ('"PoohBear" | to_screaming_snake_case', 'POOH_BEAR'),
        ]

        for expression, expected in cases:
            with self.subTest(expression):
                self._test(f'{{{{ {expression} }}}}', expected)

    @responses.activate
    def test_global_url(self):
        """Test global_url function."""
        responses.add(responses.GET, 'https://example.url/dummy', body='content')
        self._test('{{ url("https://example.url/dummy") }}\n', 'content\n')
        self._test('{{ url("https://example.url/dummy", binary=true) }}\n', "b'content'\n")

    @responses.activate
    def test_global_url_json(self):
        """Test global_url function with json content."""
        responses.add(responses.GET, 'https://example.url/dummy', json={'foo': 'bar'})
        self._test('{{ url("https://example.url/dummy") }}\n', '{"foo": "bar"}\n')
        self._test('{{ url("https://example.url/dummy", json=true) }}\n', "{'foo': 'bar'}\n")

    def test_global_is_production(self):
        """Test global is_production/deployment_environment functions."""
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test('{% if is_production %}works{% endif %}\n', 'works\n')
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test('{% if not is_production %}works{% endif %}\n', 'works\n')
        with mock.patch('cki_lib.misc.is_production_or_staging', return_value=True):
            self._test('{% if is_production_or_staging %}works{% endif %}\n', 'works\n')
        with mock.patch('cki_lib.misc.is_production_or_staging', return_value=False):
            self._test('{% if not is_production_or_staging %}works{% endif %}\n', 'works\n')
        with mock.patch('cki_lib.misc.is_staging', return_value=True):
            self._test('{% if is_staging %}works{% endif %}\n', 'works\n')
        with mock.patch('cki_lib.misc.is_staging', return_value=False):
            self._test('{% if not is_staging %}works{% endif %}\n', 'works\n')
        with mock.patch('cki_lib.misc.deployment_environment', return_value='foo'):
            self._test('{{ deployment_environment }}\n', 'foo\n')

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
            variables: typing.Dict[str, typing.Dict[str, typing.Any]],
    ) -> typing.Iterator[str]:
        secrets._read_secrets_file.cache_clear()
        secrets.read_secrets_file.cache_clear()
        secrets.read_vars_file.cache_clear()
        with tempfile.TemporaryDirectory() as directory:
            for name, values in variables.items():
                pathlib.Path(directory, name).write_text(yaml.dump(values))
            yield directory

    def test_cki_variable(self):
        """Test cki_variable function."""
        with self._setup_secrets({
                'vars.yml': {'bar': 'baz'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/vars.yml',
        }):
            self._test('{{ cki_variable("bar") }}\n', 'baz\n')

    @responses.activate
    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    def test_cki_secret(self):
        """Test cki_secret function."""
        responses.get('https://host/v1/apps/data/cki/pooh',
                      json={'data': {'data': {'value': 'bear'}}})
        with self._setup_secrets({
                'secrets.yml': {'pooh': {'backend': 'hv'}},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self._test('{{ cki_secret("pooh") }}\n', 'bear\n')

    @responses.activate
    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    def test_bucket_spec(self):
        """Test bucket_spec function."""
        responses.get('https://host/v1/apps/data/cki/pooh',
                      json={'data': {'data': {'value': 'bear'}}})
        cases = (
            ('anonymous', 'bucket_spec("bear")', {},
             'https://host|||bucket|p/refix\n'),
            ('creds', 'bucket_spec("bear", "pooh")', {},
             'https://host|id|bear|bucket|p/refix\n'),
            ('custom endpoint', 'bucket_spec("bear", "pooh")', {'endpoint_url': 'https://host'},
             'https://host|id|bear|bucket|p/refix\n'),
            ('wrong endpoint', 'bucket_spec("bear", "pooh")', {'endpoint_url': 'https://wrong'},
             None),
        )
        for description, jinja, meta, expected in cases:
            with self.subTest(description), self._setup_secrets({
                    'vars.yml': {'bear': 'https://host/bucket/p/refix'},
                    'secrets.yml': {'pooh': {'meta': {
                        'access_key_id': 'id',
                        'deployed': True,
                    } | meta}},
            }) as directory, mock.patch.dict(os.environ, {
                'CKI_VARS_FILE': f'{directory}/vars.yml',
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                self._test(f'{{{{ {jinja} }}}}\n', expected)

    @mock.patch.dict('os.environ', {'FOOBAR': 'BARBAR'})
    def test_render_nodata(self):
        """Test render function without a data file."""
        self._test('foo\n{{ env["FOOBAR"] }}\n', 'foo\nBARBAR\n')

    def test_render_noenv(self):
        """Test non-existing env variables raise an exception."""
        self._test('foo\n{{ env["FOOBAR"] }}\n', None)

    @mock.patch.dict('os.environ', {'FOOBAR': 'BARBAR'})
    def test_render(self):
        """Test render function."""
        self._test(
            'foo\n'
            '{{ data.foo }}\n'
            '{{ data.bar }}\n'
            '{{ data.bar|env }}\n'
            '{{ env["FOOBAR"] }}\n',
            'foo\n'
            'bar\n'
            '$FOOBAR\n'
            'BARBAR\n'
            'BARBAR\n',
            data_files={'data.yml': 'foo: bar\nbar: $FOOBAR'},
            data={'data': 'data.yml'},
        )

    def test_main_data_directory(self):
        """Test data directories."""
        self._test(
            '{{ data1.foo }}\n'
            '{{ data2.file1.foo }}\n'
            '{{ data2.dir1.file2.foo }}\n'
            '{{ data2.dir1.file3.foo }}\n'
            '{{ data2.dir1.file4.foo }}\n'
            '{{ data3["file1.raw"] }}\n'
            '{{ data3.dir2["file2.yml"] }}\n'
            '{{ data3.dir2["file3.yaml"] }}\n'
            '{{ data3.dir2["file4.json"] }}\n'
            '{{ data3.dir2["file5.json"] }}\n'
            '{{ data4 | tojson }}\n',
            'bar1\n'
            'bar2\nbar3\nbar4\nbar5\n'
            'raw-data-1\n"bar3"\n"bar4"\n"bar5"\n"bar6"\n'
            '{}\n',
            data_files={
                'data-file.yml': 'foo: bar1',
                'data-dir/file1.yml': '{ "foo": "bar2" }',
                'data-dir/dir1/file2.yml': '{ "foo": "bar3" }',
                'data-dir/dir1/file3.yaml': '{ "foo": "bar4" }',
                'data-dir/dir1/file4.json': '{ "foo": "bar5" }',
                'data-dir/dir1/file5.raw': '{ invalid json/yaml',
                'data-dir-2/file1.raw': 'raw-data-1',
                'data-dir-2/dir2/file2.yml': '"bar3"',
                'data-dir-2/dir2/file3.yaml': '"bar4"',
                'data-dir-2/dir2/file4.json': '"bar5"',
                'data-dir-2/dir2/file5.json.j2': '"{{ "bar6" }}"',
            },
            data={
                'data1': 'data-file.yml',
                'data2': 'data-dir',
            },
            raw_data={
                'data3': 'data-dir-2',
                'data4': 'non-existent/',
            },
        )

    def test_main_data_templates(self):
        """Test data file templating."""
        self._test(
            '{{ data1.foo }}\n'
            '{{ data2.file1.foo }}\n'
            '{{ data3 }}\n',
            'bar\n'
            'baz\n'
            'foo: bar\n',
            data_files={
                'data-file.yml.j2': 'foo: {{ "bar" }}',
                'data-dir/file1.json.j2': '{ "foo": "{{ "baz" }}" }',
            },
            data={
                'data1': 'data-file.yml.j2',
                'data2': 'data-dir',
            },
            raw_data={
                'data3': 'data-file.yml.j2',
            },
        )

    def test_main_includes(self):
        """Test jinja2 template including."""
        self._test(
            '{% include "data1.txt" %}\n',
            'foo\n',
            data_files={
                'data1.txt': '{{ "foo" }}',
            },
            include_paths=['.'],
        )

    def test_data_references_in_raw(self):
        """Test variable persistence across data files."""
        self._test(
            '{{ data2 }}\n',
            'bar\n',
            data_files={
                'data-file.yml.j2': 'foo: bar',
                'raw-data-file.txt.j2': '{{ data1.foo }}',
            },
            data={
                'data1': 'data-file.yml.j2',
            },
            raw_data={
                'data2': 'raw-data-file.txt.j2',
            },
        )

    def test_do(self):
        """Test 'do' tag works as expected."""
        self._test(
            '{%- set some_list = [] %}'
            '{% do some_list.append("foo") %}'
            '{{ some_list }}',
            '[\'foo\']',
        )

    def test_package(self):
        """Test loading templates from packages works."""
        with tempfile.TemporaryDirectory() as temp_directory:
            temp_dir = pathlib.Path(temp_directory)

            output_file = temp_dir / 'output.txt'

            args = ['render-template.j2',
                    '--no-autoescape',
                    '--package', 'tests.assets',
                    '--output', output_file.as_posix()]

            render.main(args)
            self.assertEqual(output_file.read_text(), 'foo\n')

    def test_stdin(self):
        """Test reading the template from stdin works."""
        self._test(
            '{{ "foo" }}\n',
            'foo\n',
            stdin=True,
        )

    def test_arg(self):
        """Test arguments."""
        self._test(
            '{{ foo }}\n',
            'bar\n',
            arg={'foo': 'bar'},
        )

    def test_escape(self):
        """Test escaping."""
        cli_variations = (['--no-autoescape'], ['--autoescape'])
        cases = (
            ('{{ "pooh&bear" }}',
             r'pooh&bear', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | safe }}',
             r'pooh&bear', r'pooh&bear'),
            ('{{ "pooh&bear" | e }}',
             r'pooh&amp;bear', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | escape }}',
             r'pooh&amp;bear', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | forceescape }}',
             r'pooh&amp;bear', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | safe | escape }}',
             r'pooh&bear', r'pooh&bear'),
            ('{{ "pooh&bear" | safe | forceescape }}',
             r'pooh&amp;bear', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | e | escape }}',
             r'pooh&amp;bear', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | e | forceescape }}',
             r'pooh&amp;amp;bear', r'"\"pooh\\u0026bear\""'),
            ('{{ "pooh&bear" | tojson }}',
             r'"pooh\u0026bear"', r'"\"pooh\\u0026bear\""'),
            ('{{ "pooh&bear" | tojson | safe }}',
             r'"pooh\u0026bear"', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | tojson | e }}',
             r'"pooh\u0026bear"', r'"\"pooh\\u0026bear\""'),
            ('{{ "pooh&bear" | tojson | escape }}',
             r'"pooh\u0026bear"', r'"\"pooh\\u0026bear\""'),
            ('{{ "pooh&bear" | tojson | forceescape }}',
             r'&#34;pooh\u0026bear&#34;', r'"\"pooh\\u0026bear\""'),
            ('{{ "pooh&bear" | tojson | safe | escape }}',
             r'"pooh\u0026bear"', r'"pooh\u0026bear"'),
            ('{{ "pooh&bear" | tojson | safe | forceescape }}',
             r'&#34;pooh\u0026bear&#34;', r'"\"pooh\\u0026bear\""'),
            ('{{ "pooh&bear" | tojson | e | escape }}',
             r'"pooh\u0026bear"', r'"\"pooh\\u0026bear\""'),
        )

        for template, *expected in cases:
            for cli_args, expected_output in zip(cli_variations, expected):
                with self.subTest(template, cli_args=cli_args):
                    self._test(template, expected_output, cli_args=cli_args)
