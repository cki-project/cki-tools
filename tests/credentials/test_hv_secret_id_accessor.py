"""Tests for credential management."""

import json
import os
import unittest
from unittest import mock

import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @responses.activate
    def test_create(self) -> None:
        """Test behavior of create."""

        vault_url = responses.put('https://vault/v1/apps/data/cki/secret_token')
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        accessor_url = responses.post(
            'https://vault/v1/auth/approle/role/role_name/secret-id',
            json={'data': {'secret_id_accessor': 'accessor', 'secret_id': 'secret'}})
        lookup_url = responses.post(
            'https://vault/v1/auth/approle/role/role_name/secret-id-accessor/lookup',
            json={'data': {'creation_time': '2000-01-01T00:00:00.0+00:00Z'}})

        meta = {
            'active': True,
            'deployed': True,
            'role_id': 'role_name',
            'token_type': 'hv_secret_id_accessor',
            'vault_addr': 'https://vault',
        }
        expected = {
            'created_at': '2000-01-01T00:00:00.0+00:00Z',
            'secret_id_accessor': 'accessor',
        }

        with setup_secrets({'secret_token': {'backend': 'hv', 'meta': meta}}):
            manager.main(['create', '--token-name', 'secret_token'])
            self.assertEqual(json.loads(vault_url.calls[0].request.body),
                             {'data': {'value': 'secret'}})
            self.assertEqual(secrets.secret('secret_token#'), meta | expected)
            self.assertEqual(len(accessor_url.calls), 1)
            self.assertEqual(len(lookup_url.calls), 1)
            self.assertEqual(accessor_url.calls[0].request.headers['X-Vault-Token'], 'vault_token')
            self.assertEqual(json.loads(lookup_url.calls[0].request.body),
                             {'secret_id_accessor': 'accessor'})
            self.assertEqual(lookup_url.calls[0].request.headers['X-Vault-Token'], 'vault_token')

    @responses.activate
    def test_destroy(self) -> None:
        """Test behavior of destroy."""

        destroy_url = responses.post(
            'https://vault/v1/auth/approle/role/role_name/secret-id-accessor/destroy', json={})

        meta = {
            'active': True,
            'created_at': '2000-01-01T00:00:00.0+00:00Z',
            'deployed': False,
            'role_id': 'role_name',
            'secret_id_accessor': 'accessor',
            'token_type': 'hv_secret_id_accessor',
            'vault_addr': 'https://vault',
        }

        with setup_secrets({'secret_token': {'backend': 'hv', 'meta': meta}}):
            manager.main(['destroy', '--token-name', 'secret_token'])
            self.assertEqual(secrets.secret('secret_token#active'), False)
            self.assertEqual(len(destroy_url.calls), 1)
            self.assertEqual(json.loads(destroy_url.calls[0].request.body),
                             {'secret_id_accessor': 'accessor'})
            self.assertEqual(destroy_url.calls[0].request.headers['X-Vault-Token'], 'vault_token')

    @responses.activate
    def test_validate(self) -> None:
        """Test behavior of validate."""
        login_url = responses.post('https://vault/v1/auth/approle/login', json={})

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        meta = {
            'active': True,
            'created_at': '2000-01-01T00:00:00.0+00:00Z',
            'deployed': True,
            'role_id': 'role_name',
            'secret_id_accessor': 'accessor',
            'token_type': 'hv_secret_id_accessor',
            'vault_addr': 'https://vault',
        }

        with setup_secrets({'secret_token': {'backend': 'hv', 'meta': meta}}):
            manager.main(['validate', '--token-name', 'secret_token'])
            self.assertEqual(len(login_url.calls), 1)
            self.assertEqual(json.loads(login_url.calls[0].request.body),
                             {'role_id': 'role_name', 'secret_id': 'secret'})
