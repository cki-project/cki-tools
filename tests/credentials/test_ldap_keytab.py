"""Tests for credential management."""

import os
import unittest
from unittest import mock

from cki_tools.credentials import ldap_helpers
from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    def setUp(self) -> None:
        """Set up test environment."""
        ldap_helpers.ldap_search.cache_clear()

    @mock.patch('ldap.initialize')
    def test_update(self, ldap_initialize) -> None:
        """Test behavior of update."""
        def mock_search(dn, *_, **__):
            match dn:
                case 'some=dn':
                    return [(dn, {
                        'memberOf': [b'foo=1', b'bar=2', b'baz=3'],
                        'cn': [b'common'],
                    })]
                case 'foo=1':
                    return [(dn, {'rhatRoverGroupMemberQuery': [b'query']})]
                case 'bar=2':
                    return [(dn, {})]
                case 'baz=3':
                    return [(dn, {})]
        ldap_initialize.return_value.search_s.side_effect = mock_search

        token = 'secret_token'
        meta = {
            'active': True,
            'dn': 'some=dn',
            'ldap_server': 'ldap.server',
            'token_type': 'ldap_keytab',
        }
        expected = {
            'cn': 'common',
            'memberOf': ['bar=2', 'baz=3'],
        }

        with setup_secrets({'secret_token': {'meta': meta}}):
            manager.main(['update', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)
