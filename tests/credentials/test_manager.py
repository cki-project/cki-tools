"""Tests for credential management."""

import os
import unittest
from unittest import mock

from freezegun import freeze_time
import responses

from cki_tools.credentials import manager

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestManager(unittest.TestCase):
    """Tests for credential management."""
    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_create(self) -> None:
        """Test behavior of manager.create."""
        token = 'secret_token'
        cases = (
            ('token not supported', ['--token-name', token], {
                'token_type': 'pooh_bear',
            }, 'Unknown'),
            ('create not supported', ['--token-name', token], {
                'token_type': 'splunk_hec_token',
            }, 'create'),
            ('single token', [], {
                'token_type': 'splunk_hec_token',
            }, None),
        )
        for description, args, meta, expected_exception in cases:
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                if not expected_exception:
                    self.assertEqual(manager.main(['create'] + args), 1)
                else:
                    with self.assertRaises(Exception) as ex:
                        manager.main(['create'] + args)
                    self.assertIn(expected_exception, str(ex.exception))

    def test_destroy(self) -> None:
        """Test behavior of manager.destroy."""
        token = 'secret_token'
        cases = (
            ('token not supported', ['--token-name', token], {
                'token_type': 'pooh_bear',
            }, 'Unknown'),
            ('destroy not supported', ['--token-name', token], {
                'deployed': False,
                'token_type': 'splunk_hec_token',
            }, 'destroy'),
            ('single token', [], {
                'token_type': 'splunk_hec_token',
            }, None),
        )
        for description, args, meta, expected_exception in cases:
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                if not expected_exception:
                    self.assertEqual(manager.main(['destroy'] + args), 1)
                else:
                    with self.assertRaises(Exception) as ex:
                        manager.main(['destroy'] + args)
                    self.assertIn(expected_exception, str(ex.exception))

    def test_rotate(self) -> None:
        """Test behavior of manager.rotate."""
        token = 'secret_token'
        cases = (
            ('token not supported', ['--token-name', token], {
                'token_type': 'pooh_bear',
            }, 'Unknown'),
            ('single token', [], {
                'token_type': 'splunk_hec_token',
            }, None),
        )
        for description, args, meta, expected_exception in cases:
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                if not expected_exception:
                    self.assertEqual(manager.main(['rotate'] + args), 1)
                else:
                    with self.assertRaises(Exception) as ex:
                        manager.main(['rotate'] + args)
                    self.assertIn(expected_exception, str(ex.exception))

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_update(self) -> None:
        """Test behavior of manager.update."""
        token = 'secret_token'
        meta = {
            'token_type': 'pooh_bear',
        }
        with setup_secrets({token: {'meta': meta}}):
            self.assertEqual(manager.main(['update']), 1)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_validate(self) -> None:
        """Test behavior of manager.validate."""
        token = 'secret_token'
        meta = {
            'token_type': 'pooh_bear',
        }
        with setup_secrets({token: {'meta': meta}}):
            self.assertEqual(manager.main(['validate']), 1)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_purge(self) -> None:
        """Test behavior of manager.purge."""
        token = 'secret_token'
        cases = (
            (None, {
                'active': False,
                'deployed': False,
                'token_type': 'password',
            }),
            ('active', {
                'active': True,
                'deployed': False,
                'token_type': 'password',
            }),
            ('unknown', {
                'active': False,
                'deployed': False,
                'token_type': 'pooh_bear',
            }),
        )
        for description, meta in cases:
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                self.assertEqual(manager.main(['purge']), 1 if description else 0)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_status(self) -> None:
        """Test behavior of manager.status."""
        token = 'secret_token'
        meta = {
            'token_type': 'pooh_bear',
        }
        with setup_secrets({token: {'meta': meta}}):
            self.assertEqual(manager.main(['status']), 1)

    @mock.patch("builtins.input")
    def test_prepare(self, mock_input) -> None:
        """Test behavior of manager.prepare."""
        token = 'secret_token'
        cases = {
            "no name": (["--force"], {"token_type": "password"}, None),
        }
        for description, (args, meta, expected_exception) in cases.items():
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                if not expected_exception:
                    self.assertEqual(manager.main(['prepare'] + args), 1)
                else:
                    with self.assertRaises(Exception) as ex:
                        manager.main(['prepare'] + args)
                    self.assertIn(expected_exception, str(ex.exception))

    @mock.patch("builtins.input")
    def test_switch(self, mock_input) -> None:
        """Test behavior of manager.switch."""
        token = 'secret_token'
        cases = (
            ('no name', ['--force'], {
                'token_type': 'password',
            }, None),
        )
        for description, args, meta, expected_exception in cases:
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                if not expected_exception:
                    self.assertEqual(manager.main(['switch'] + args), 1)
                else:
                    with self.assertRaises(Exception) as ex:
                        manager.main(['switch'] + args)
                    self.assertIn(expected_exception, str(ex.exception))

    @mock.patch("builtins.input")
    def test_clean(self, mock_input) -> None:
        """Test behavior of manager.clean."""
        token = 'secret_token'
        cases = (
            ('no name', ['--force'], {
                'token_type': 'password',
            }, None),
        )
        for description, args, meta, expected_exception in cases:
            with self.subTest(description), setup_secrets({token: {'meta': meta}}):
                if not expected_exception:
                    self.assertEqual(manager.main(['clean'] + args), 1)
                else:
                    with self.assertRaises(Exception) as ex:
                        manager.main(['clean'] + args)
                    self.assertIn(expected_exception, str(ex.exception))
