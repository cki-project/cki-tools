"""Tests for credential management."""

import copy
import datetime
import json
import os
import textwrap
import unittest
from unittest import mock

from cki_lib import misc
import freezegun
import responses

from cki_tools.credentials import aws_tokens
from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    def setUp(self) -> None:
        """Test setup code."""
        aws_tokens._iam.cache_clear()

    @mock.patch("cki_tools.credentials.aws_tokens.boto3.Session")
    @responses.activate
    def test_create(self, boto_session) -> None:
        """Test behavior of create."""
        create_date = datetime.datetime(2010, 1, 1)
        boto_session.return_value.client.return_value.create_access_key.return_value = {
            "AccessKey": {
                "AccessKeyId": "aki",
                "SecretAccessKey": "secret",
                "CreateDate": create_date,
            }
        }

        vault_url = responses.put('https://vault/v1/apps/data/cki/secret_token')
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        token = 'secret_token'
        meta = {
            "account": "account",
            "token_type": "aws_secret_access_key",
            "profile_name": "testing_admin",
            "user_name": "username",
        }
        expected = {
            'active': True,
            'access_key_id': 'aki',
            'arn': 'arn:aws:iam::account:user/username',
            'created_at': '2010-01-01T00:00:00+00:00',
            'deployed': True,
        }

        with setup_secrets({token: {'meta': meta}}):
            manager.main(['create', '--token-name', token])

            self.assertEqual(
                boto_session.mock_calls,
                (
                    mock.call(profile_name="testing_admin")
                    .client("iam")
                    .create_access_key(UserName="username")
                    .call_list()
                ),
            )

            self.assertEqual(json.loads(vault_url.calls[0].request.body),
                             {'data': {'value': 'secret'}})
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @mock.patch("cki_tools.credentials.aws_tokens.boto3.Session")
    def test_destroy(self, boto_session) -> None:
        """Test behavior of destroy."""

        token = 'secret_token'
        meta = {
            "access_key_id": "aki",
            "account": "account",
            "profile_name": "testing_admin",
            "active": True,
            "arn": "arn",
            "created_at": "2010-01-01T00:00:00+00:00",
            "deployed": False,
            "token_type": "aws_secret_access_key",
            "user_name": "username",
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(["destroy", "--token-name", token])

            self.assertEqual(
                boto_session.mock_calls,
                (
                    mock.call(profile_name="testing_admin")
                    .client("iam")
                    .delete_access_key(UserName="username", AccessKeyId="aki")
                    .call_list()
                ),
            )

    @mock.patch("cki_tools.credentials.aws_tokens.boto3.Session")
    @responses.activate
    def test_update(self, boto_session) -> None:
        """Test behavior of update."""
        create_date = datetime.datetime(2010, 1, 1)
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        boto_session.return_value.client.return_value.get_access_key_last_used.return_value = {
            "UserName": "username",
        }
        boto_session.return_value.client.return_value.list_access_keys.return_value = {
            "AccessKeyMetadata": [
                {"AccessKeyId": "aki", "Status": "Active", "CreateDate": create_date},
            ]
        }

        base_meta = {
            "access_key_id": "aki",
            "active": True,
            "deployed": True,
            "token_type": "aws_secret_access_key",
            "profile_name": "testing_admin",
        }

        sts_client_access_key = mock.call().client(
            "sts", aws_access_key_id="aki", aws_secret_access_key="secret"
        )
        sts_client_profile_name = mock.call(profile_name="testing_admin").client("sts")
        iam_client_profile_name = mock.call(profile_name="testing_admin").client("iam")

        cases = {
            "default": (
                "account",
                {},
                {
                    "account": "account",
                    "arn": "arn",
                    "created_at": "2010-01-01T00:00:00+00:00",
                    "user_name": "username",
                },
                [
                    *sts_client_access_key.get_caller_identity().call_list(),
                    *sts_client_profile_name.get_caller_identity().call_list(),
                    *iam_client_profile_name.call_list(),
                    iam_client_profile_name.get_access_key_last_used(AccessKeyId="aki"),
                    iam_client_profile_name.list_access_keys(UserName="username"),
                ],
            ),
            "known account, arn": (
                "other-account",
                {
                    "account": "other-account",
                    "arn": "other-arn",
                },
                {
                    "account": "other-account",
                    "arn": "other-arn",
                    "created_at": "2010-01-01T00:00:00+00:00",
                    "user_name": "username",
                },
                [
                    *sts_client_profile_name.get_caller_identity().call_list(),
                    *iam_client_profile_name.call_list(),
                    iam_client_profile_name.get_access_key_last_used(AccessKeyId="aki"),
                    iam_client_profile_name.list_access_keys(UserName="username"),
                ],
            ),
            "known account, arn, user_name": (
                "other-account",
                {
                    "account": "other-account",
                    "arn": "other-arn",
                    "user_name": "other-username",
                    "profile_name": "testing_admin",
                },
                {
                    "account": "other-account",
                    "arn": "other-arn",
                    "created_at": "2010-01-01T00:00:00+00:00",
                    "user_name": "other-username",
                },
                [
                    *sts_client_profile_name.get_caller_identity().call_list(),
                    *iam_client_profile_name.call_list(),
                    iam_client_profile_name.list_access_keys(UserName="other-username"),
                ],
            ),
            "mismatched account ignored": (
                "account",
                {
                    "account": "other-account",
                    "arn": "other-arn",
                },
                {
                    "account": "other-account",
                    "arn": "other-arn",
                },
                [
                    *sts_client_profile_name.get_caller_identity().call_list(),
                ],
            ),
            "custom endpoint": (
                "account",
                {
                    "endpoint_url": "endpoint",
                },
                {
                    "endpoint_url": "endpoint",
                },
                [],
            ),
        }

        for description, (account, extra_meta, extra_expected, expected_calls) in cases.items():
            with (
                self.subTest(description),
                setup_secrets({"secret_token": {"backend": "hv", "meta": base_meta | extra_meta}}),
            ):
                boto_session.return_value.client.return_value.get_caller_identity.return_value = {
                    "Account": account,
                    "Arn": "arn",
                }
                manager.main(['update', '--token-name', 'secret_token'])

                self.assertEqual(secrets.secret("secret_token#"), base_meta | extra_expected)

                self.assertEqual(boto_session.mock_calls, expected_calls)

            boto_session.reset_mock()
            aws_tokens._iam.cache_clear()
            aws_tokens._current_account.cache_clear()

    @freezegun.freeze_time("2000-01-03T00:00:00.0+00:00")
    @responses.activate
    @mock.patch("cki_tools.credentials.aws_tokens.boto3.Session")
    @mock.patch("builtins.input")
    def test_prepare(self, mock_input, boto_session):
        """Test AwsSecretAccessKey().prepare()."""
        new_secret = "secret"
        new_access_key = "c-aki"
        boto_session.return_value.client.return_value.create_access_key.return_value = {
            "AccessKey": {
                "AccessKeyId": new_access_key,
                "SecretAccessKey": new_secret,
                "CreateDate": misc.now_tz_utc(),
            }
        }

        token_name = "secret_token"
        token_name_old = f"{token_name}/915148800"
        token_name_new = f"{token_name}/946857600"

        base_secrets = {
            token_name: {
                "backend": "hv",
                "meta": {
                    "access_key_id": "a-aki",
                    "account": "account",
                    "active": True,
                    "arn": "arn:aws:iam::account:user/username",
                    "created_at": "2000-01-01T00:00:00+00:00",  # Note the deployed creation date
                    "deployed": True,
                    "profile_name": "testing_admin",
                    "token_type": "aws_secret_access_key",
                    "user_name": "username",
                },
            },
            token_name_old: {
                "backend": "hv",
                "meta": {
                    "access_key_id": "b-aki",
                    "account": "account",
                    "arn": "arn:aws:iam::account:user/username",
                    "deployed": False,
                    "profile_name": "testing_admin",
                    "token_type": "aws_secret_access_key",
                    "user_name": "username",
                },
            },
        }
        cases = {
            "Need to prepare: Less than 2 active tokens with profile_name": {
                "extra_meta": {"active": False},
                "expected_calls": 1,
            },
            "Need to prepare: active is older than deployed": {
                "extra_meta": {"active": True, "created_at": "1999-01-01T00:00:00+00:00"},
                "expected_calls": 1,
            },
            "No need to prepare: active token created after deployed. Do not force": {
                "extra_meta": {"active": True, "created_at": "2000-01-03T00:00:00+00:00"},
                "expected_calls": 0,
            },
            "No need to prepare: active token created after deployed. Force; do not confirm": {
                "extra_meta": {"active": True, "created_at": "2000-01-03T00:00:00+00:00"},
                "extra_args": ["--force"],
                "confirm": ["yes", "no"],
                "expected_calls": 0,
            },
            "No need to prepare: active token created after deployed. Force; confirm": {
                "extra_meta": {"active": True, "created_at": "2000-01-03T00:00:00+00:00"},
                "extra_args": ["--force"],
                "confirm": ["yes", "y"],
                "expected_calls": 1,
            },
            "No need to prepare: Missing profile_name; do not force": {
                "extra_meta": {"active": True, "profile_name": ""},
                "expected_calls": 0,
            },
        }
        vault_url = responses.put(f"https://vault/v1/apps/data/cki/{token_name_new}")
        responses.get(
            f"https://vault/v1/apps/data/cki/{token_name_new}",
            json={"data": {"data": {"value": new_secret}}},
        )

        for description, case_kwargs in cases.items():
            mocked_secrets = copy.deepcopy(base_secrets)
            mocked_secrets[token_name_old]["meta"] |= case_kwargs["extra_meta"]
            # default to mock input the first prompt, telling how many tokens will be affected
            mock_input.side_effect = case_kwargs.get("confirm", ["y"])
            # optional args to pass to the function via CLI
            extra_args = case_kwargs.get("extra_args", [])

            with self.subTest(description), setup_secrets(mocked_secrets):
                manager.main(["prepare", *extra_args])

                mock_input.assert_any_call(
                    textwrap.dedent(
                        """\
                        You are about to update 1 tokens:
                            - secret_token
                        Are you sure you want to proceed? [y/N]: """
                    )
                )
                if "--force" in extra_args:
                    mock_input.assert_any_call("Force prepare [y/N]: ")

                self.assertEqual(len(vault_url.calls), case_kwargs["expected_calls"])
                if not case_kwargs["expected_calls"]:
                    # prepare was skipped
                    self.assertTrue(secrets.secret(f"{token_name_old}#active"))
                    continue

                iam_client = mock.call(profile_name="testing_admin").client("iam")
                expected_calls = iam_client.create_access_key(UserName="username").call_list()
                # when the non-deployed token is active, it should be deleted during prepare
                if misc.get_nested_key(case_kwargs, "extra_meta/active"):
                    expected_calls.append(
                        iam_client.delete_access_key(UserName="username", AccessKeyId="b-aki")
                    )
                self.assertEqual(boto_session.mock_calls, expected_calls)

                # secret value updated in vault
                self.assertEqual(
                    json.loads(vault_url.calls[0].request.body), {"data": {"value": new_secret}}
                )
                # meta value updated in file
                self.assertFalse(secrets.secret(f"{token_name_old}#active"))
                self.assertEqual(
                    secrets.secret(f"{token_name_new}#"),
                    {
                        "access_key_id": new_access_key,
                        "account": "account",
                        "active": True,  # Note it is active
                        "arn": "arn:aws:iam::account:user/username",
                        "created_at": misc.now_tz_utc().isoformat(),
                        "deployed": False,
                        "profile_name": "testing_admin",
                        "token_type": "aws_secret_access_key",
                        "user_name": "username",
                    },
                )

            # setup subtest
            boto_session.reset_mock()
            vault_url.calls.reset()
            aws_tokens._iam.cache_clear()

    @freezegun.freeze_time("2000-01-03T00:00:00.0+00:00")
    @responses.activate
    @mock.patch("builtins.input")
    def test_switch(self, mock_input):
        """Test AwsSecretAccessKey().switch()."""
        token_name_old = "secret_token"
        token_name_new = f"{token_name_old}/946857600"

        base_secrets = {
            token_name_old: {
                "backend": "hv",
                "meta": {
                    "access_key_id": "a-aki",
                    "account": "account",
                    "active": True,
                    "arn": "arn:aws:iam::account:user/username",
                    "created_at": "2000-01-01T00:00:00+00:00",
                    "expires_at": None,  # Not really used currently, but supported
                    "deployed": True,
                    "profile_name": "testing_admin",
                    "token_type": "aws_secret_access_key",
                    "user_name": "username",
                },
            },
            token_name_new: {
                "backend": "hv",
                "meta": {
                    "access_key_id": "b-aki",
                    "account": "account",
                    "active": True,
                    "arn": "arn:aws:iam::account:user/username",
                    "created_at": "2000-01-03T00:00:00+00:00",
                    "deployed": False,
                    "profile_name": "testing_admin",
                    "token_type": "aws_secret_access_key",
                    "user_name": "username",
                },
            },
        }
        cases = {
            "Not switching because it needs preparation: Less than 2 active tokens": {
                "extra_meta": {"active": False},
                "expected_calls": 0,
            },
            "Need to switch: deployed is too old": {
                "extra_meta": {"active": True, "created_at": "1999-01-01T00:00:00+00:00"},
                "expected_calls": 1,
            },
            "Need to switch: deployed is close to expiry": {
                "extra_meta": {"active": True, "expires_at": "2000-02-01T00:00:00+00:00"},
                "expected_calls": 1,
            },
            "No need to switch: deployed is young. Do not force": {
                "extra_meta": {"active": True, "created_at": "2000-01-03T00:00:00+00:00"},
                "expected_calls": 0,
            },
            "No need to switch: deployed is young. Force; do not confirm": {
                "extra_meta": {"active": True, "created_at": "2000-01-01T00:00:00+00:00"},
                "extra_args": ["--force"],
                "confirm": ["yes", "no"],
                "expected_calls": 0,
            },
            "No need to switch: deployed is young. Force; confirm": {
                "extra_meta": {"active": True, "created_at": "2000-01-01T00:00:00+00:00"},
                "extra_args": ["--force"],
                "confirm": ["yes", "y"],
                "expected_calls": 1,
            },
            "No need to rotate: Missing profile_name; do not force": {
                "extra_meta": {"active": True, "profile_name": ""},
                "expected_calls": 0,
            },
        }

        for description, case_kwargs in cases.items():
            mocked_secrets = copy.deepcopy(base_secrets)
            mocked_secrets[token_name_old]["meta"] |= case_kwargs["extra_meta"]
            # default to mock input the first prompt, telling how many tokens will be affected
            mock_input.side_effect = case_kwargs.get("confirm", ["y"])
            # optional args to pass to the function via CLI
            extra_args = case_kwargs.get("extra_args", [])

            with self.subTest(description), setup_secrets(mocked_secrets):
                manager.main(["switch", *extra_args])

                mock_input.assert_any_call(
                    textwrap.dedent(
                        """\
                        You are about to update 1 tokens:
                            - secret_token
                        Are you sure you want to proceed? [y/N]: """
                    )
                )
                if "--force" in extra_args:
                    mock_input.assert_any_call("Force switch [y/N]: ")

                if not case_kwargs["expected_calls"]:
                    # switch was skipped
                    self.assertTrue(secrets.secret(f"{token_name_old}#deployed"))
                    self.assertFalse(secrets.secret(f"{token_name_new}#deployed"))
                    continue

                # meta value updated in file
                self.assertFalse(secrets.secret(f"{token_name_old}#deployed"))
                self.assertTrue(secrets.secret(f"{token_name_new}#deployed"))
