"""Tests for credential management."""

import unittest
from unittest import mock

from cki_tools.credentials import utils

from .utils import setup_secrets


class TestCredentialManager(unittest.TestCase):
    """Tests for credential management."""

    def test_all_tokens(self) -> None:
        """Test behavior of all_tokens."""
        cases = (
            ('default', ['t1', 't2'], {'t1'}, ['0']),
            ('multiple', ['t1'], {'t1', 't2'}, ['0']),
        )
        for description, tokens, types, expected in cases:
            with self.subTest(description), setup_secrets({
                str(i): {'meta': {'token_type': t}} for i, t in enumerate(tokens)
            }):
                self.assertEqual(utils.all_tokens(types), expected)

    @mock.patch("builtins.input")
    def test_confirm(self, mock_input):
        """Test utils.confirm()."""
        test_cases = {
            "y": True,
            "yes": True,
            "Y": True,
            "YES": True,
            "n": False,
            "no": False,
            "N": False,
            "NO": False,
            "": False,
            "random": False,
        }

        for user_input, expected in test_cases.items():
            with self.subTest(input=user_input, expected=expected):
                mock_input.return_value = user_input
                self.assertEqual(utils.confirm("Continue?"), expected)
                mock_input.assert_called()
