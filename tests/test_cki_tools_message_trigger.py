"""Test cki_tools.message_trigger."""

from collections import abc
from contextlib import contextmanager
from contextlib import nullcontext
import json
import pathlib
import tempfile
import unittest
from unittest import mock

from cki_lib import yaml
import responses

from cki_tools.message_trigger import main


class TestTrigger(unittest.TestCase):
    """Test cki_tools.message_trigger."""

    def test_condition_result(self) -> None:
        """Test condition matching."""
        data = {
            'pooh': {'bear': 'crow', 'lines': 'multi\nline'},
        }
        cases = (
            ('existing', {'jmespath': 'pooh.bear'}, True, 'crow', {}),
            ('serialized', {'jmespath': 'pooh'}, True, json.dumps(data['pooh']), {}),
            ('missing', {'jmespath': 'missing'}, False, None, {}),
            ('matching regex', {'jmespath': 'pooh.bear', 'regex': 'crow'}, True, 'crow', {}),
            ('not matching', {'jmespath': 'pooh.bear', 'regex': 'bar'}, False, 'crow', {}),
            ('capture', {'jmespath': 'pooh.bear', 'regex': '(?P<first_letter>.)row'}, True, 'crow',
             {'first_letter': 'c'}),
            ('multiline', {'jmespath': 'pooh.lines', 'regex': '(?P<all>.+)'}, True, 'multi\nline',
             {'all': 'multi\nline'}),
            ('multiline enabled', {'jmespath': 'pooh.lines', 'regex': '(?s:(?P<all>.+))'}, True,
             'multi\nline', {'all': 'multi\nline'}),
            ('multiline disabled', {'jmespath': 'pooh.lines', 'regex': '(?-s:(?P<all>.+))'}, False,
             'multi\nline', {}),
        )
        for description, config, expected_is_match, expected_value, expected_captures in cases:
            with self.subTest(description):
                match_result = main.MatchConditionResult(main.MatchCondition(**config), data)
                self.assertEqual(match_result.is_match, expected_is_match)
                self.assertEqual(match_result.value, expected_value)
                self.assertEqual(match_result.captures, expected_captures)

    def test_config_result(self) -> None:
        """Test match results and variables."""
        data = {
            'pooh': {'bear': 'crow'},
        }
        cases = (
            ('matching', {
                'name': 'config',
                'project_url': 'project',
                'ref': 'ref',
                'conditions': {
                    'a': {'jmespath': 'pooh.bear', 'regex': '(?P<all>.+)'},
                    'b': {'jmespath': "pooh.bear == 'crow'", 'regex': '(?P<is_crow>.+)'},
                    'c': {'jmespath': "pooh.bear == 'bar'", 'regex': '(?P<is_bar>.+)'},
                }, 'variables': {
                    'IS_CROW': '{all} == crow: {is_crow}',
                    'IS_BAR': '{all} == bar: {is_bar}',
                },
            }, True, {
                'IS_CROW': 'crow == crow: true',
                'IS_BAR': 'crow == bar: false',
            }),
            ('missing', {
                'name': 'config',
                'project_url': 'project',
                'ref': 'ref',
                'conditions': {'a': {'jmespath': 'pooh.beaaaar'}},
            }, False, {}),
        )
        for description, config, expected_is_match, expected_variables in cases:
            with self.subTest(description):
                match_result = main.MatchConfigResult(main.MatchConfig(**config), data)
                self.assertEqual(match_result.is_match, expected_is_match)
                if expected_is_match:
                    self.assertEqual(match_result.variables, expected_variables)

    @responses.activate
    def test_handle(self) -> None:
        """Test triggering."""
        data = {
            'key': 'crowbar',
        }
        cases = ({
            'description': 'one',
            'is_production': True,
            'post_args': {},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}},
            'expected_exc': False,
            'expected_post': [{'ref': 'pooh', 'variables': []}]
        }, {
            'description': 'none',
            'is_production': True,
            'post_args': {},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh',
                               'conditions': {'a': {'jmespath': 'keyyyyy'}}}},
            'expected_exc': False,
            'expected_post': []
        }, {
            'description': 'two',
            'is_production': True,
            'post_args': {},
            'config': {
                'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}},
                'two': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}
            },
            'expected_exc': False,
            'expected_post': [{'ref': 'pooh', 'variables': []}, {'ref': 'pooh', 'variables': []}]
        }, {
            'description': 'non-prod',
            'is_production': False,
            'post_args': {},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}},
            'expected_exc': False,
            'expected_post': []
        }, {
            'description': 'vars',
            'is_production': True,
            'post_args': {},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh',
                               'conditions': {'a': {'jmespath': 'key', 'regex': '(?P<group>.+)'}},
                               'variables': {'bear': '{group}'}}},
            'expected_exc': False,
            'expected_post': [{'ref': 'pooh', 'variables': [{'key': 'bear', 'value': 'crowbar'}]}]
        }, {
            'description': '400 no jobs',
            'is_production': True,
            'post_args': {'status': 400, 'json': {'base': [
                'Pipeline will not run for the selected trigger. '
                'The rules configuration prevented any jobs from being added to the pipeline.',
            ]}},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}},
            'expected_exc': False,
            'expected_post': [{'ref': 'pooh', 'variables': []}]
        }, {
            'description': '400 no jobs',
            'is_production': True,
            'post_args': {'status': 400, 'json': {'base': [
                'The resulting pipeline would have been empty. '
                'Review the rules configuration for the relevant jobs.',
            ]}},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}},
            'expected_exc': False,
            'expected_post': [{'ref': 'pooh', 'variables': []}]
        }, {
            'description': '400 other error',
            'is_production': True,
            'post_args': {'status': 400, 'json': None},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}},
            'expected_exc': True,
            'expected_post': [{'ref': 'pooh', 'variables': []}]
        }, {
            'description': '500 error',
            'is_production': True,
            'post_args': {'status': 500, 'json': None, 'body': 'will not run'},
            'config': {'one': {'project_url': 'http://u/project', 'ref': 'pooh', 'conditions': {}}},
            'expected_exc': True,
            'expected_post': [{'ref': 'pooh', 'variables': []}]
        })

        for case in cases:
            with (self.subTest(case['description']),
                  mock.patch('cki_lib.misc.is_production', return_value=case['is_production']),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('http://u/api/v4/projects/project', json={
                    'id': 1, 'web_url': 'http://u/project',
                })
                post = rsps.post('http://u/api/v4/projects/1/pipeline', **{'json': {
                    'id': 2, 'web_url': 'http://u/project/-/pipelines/2',
                }, **case['post_args']})

                with self.assertRaises(Exception) if case['expected_exc'] else nullcontext():
                    main.Handler([case['config']]).handle(data)

                self.assertEqual([json.loads(c.request.body)
                                 for c in post.calls], case['expected_post'])

    @staticmethod
    @contextmanager
    def files(
        files: dict[str, str],
    ) -> abc.Iterator[str]:
        """Create a prepopulated temporary directory."""
        with tempfile.TemporaryDirectory() as directory:
            for name, data in files.items():
                pathlib.Path(directory, name).write_text(data, encoding='utf8')
            yield directory

    @responses.activate
    def test_env_config(self) -> None:
        """Test env config parsing."""
        config = {"one": {"project_url": "http://u/project", "ref": "pooh", "conditions": {}}}
        config_str = yaml.dump(config)
        cases = (
            ('dir', {
                'MESSAGE_TRIGGER_CONFIG_DIR': 'DIRECTORY',
                'MESSAGE_TRIGGER_CONFIG': config_str,
                'MESSAGE_TRIGGER_CONFIG_PATH': 'DIRECTORY/config.yml',
            }, {
                'config.json': config_str,
                'config.yaml': config_str,
                'config.yml': config_str,
                'config.something': 'invalid',
            }, 3 * [config]),
            ('contents', {
                'MESSAGE_TRIGGER_CONFIG': config_str,
                'MESSAGE_TRIGGER_CONFIG_PATH': 'DIRECTORY/config.yml',
            }, {
            }, [config]),
            ('file', {
                'MESSAGE_TRIGGER_CONFIG_PATH': 'DIRECTORY/config.yml',
            }, {
                'config.yml': config_str,
            }, [config]),
        )

        for description, env, files, expected in cases:
            with (self.subTest(description), self.files(files) as directory, mock.patch.dict(
                    'os.environ', {k: v.replace('DIRECTORY', directory) for k, v in env.items()})):
                self.assertEqual(main.env_config(), expected)

    def test_validate(self) -> None:
        """Test validation."""
        cases = [
            ('capture', {'bear': {'project_url': 'http://pooh', 'ref': 'pooh', 'conditions': {
                'condition': {'jmespath': 'pooh', 'regex': '(?P<crow>.+)'},
            }, 'variables': {'BAR': '{crow}'}}}, None),
            ('invalid regex', {'bear': {'project_url': 'http://pooh', 'ref': 'pooh', 'conditions': {
                'condition': {'jmespath': 'pooh', 'regex': '(?P<crow>.+'},
            }, 'variables': {'BAR': 'crow'}}}, Exception),
            ('missing', {'bear': {'project_url': 'http://pooh', 'ref': 'pooh', 'conditions': {
                'condition': {'jmespath': 'pooh', 'regex': '(?P<crow>.+)'},
            }, 'variables': {'BAR': '{bear}'}}}, Exception),
            ('empty', {'bear': {'project_url': 'http://pooh', 'ref': 'pooh', 'conditions': {
                'condition': {'jmespath': 'pooh', 'regex': '(?P<crow>.+)'},
            }, 'variables': {'BAR': '{}'}}}, Exception),
            ('invalid', {'bear': {'project_url': 'http://pooh', 'ref': 'pooh', 'conditions': {
                'condition': {'jmespath': 'pooh', 'regex': '(?P<crow>.+)'},
            }, 'variables': {'BAR': '{'}}}, Exception),
        ]
        for description, config, expected_exc in cases:
            with (self.subTest(description),
                  mock.patch.dict('os.environ', {'MESSAGE_TRIGGER_CONFIG': json.dumps(config)}),
                  self.assertRaises(expected_exc) if expected_exc else nullcontext()):
                main.main(['--validate'])
